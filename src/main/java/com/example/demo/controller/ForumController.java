package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import org.thymeleaf.util.StringUtils;

import com.example.demo.entity.Report;
import com.example.demo.service.ReportService;
import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;

import java.sql.Timestamp;
import java.util.List;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面(TOP)
	@GetMapping
	public ModelAndView top(@RequestParam(value = "startDate", required = false) String startDate, @RequestParam(value = "endDate", required = false) String endDate) {
		ModelAndView mav = new ModelAndView();
		List<Report> contentData = null;
		if (!StringUtils.isEmpty(startDate) || !StringUtils.isEmpty(endDate)) {
			contentData = reportService.searchReport(startDate, endDate);
		} else {
			contentData = reportService.findAllReport();
		}

		mav.setViewName("/top");
		mav.addObject("contents", contentData);
		mav.addObject("startDate", startDate);
		mav.addObject("endDate", endDate);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newReport() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	//投稿詳細画面
	@GetMapping("/detail/{id}")
	public ModelAndView detailReportAndComments(@PathVariable("id") int id) {
		Report reportData = reportService.findOneReport(id);
		List<Comment> commentList = commentService.findByReportId(id);
		ModelAndView mav = new ModelAndView();
		Comment commentData = new Comment();
		mav.setViewName("/detail");
		mav.addObject("commentList", commentList);
		mav.addObject("reportData", reportData );
		mav.addObject("commentData", commentData);
		return mav;
	}

	// report投稿処理
	@PostMapping("/add")
	public ModelAndView addReport(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// report削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteReport(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	// report編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editReport(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Report report = reportService.editReport(id);
		mav.addObject("formModel", report);
		mav.setViewName("/edit");
		return mav;
	}

	// report更新処理
	@PutMapping("/update/{id}")
	public ModelAndView updateReport(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		report.setId(id);
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	// comment投稿処理
	@PostMapping("/comment")
	public ModelAndView addComment(@ModelAttribute("commentModel") Comment comment) {
		Report report = reportService.findOneReport(comment.getReportId());
		report.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
		reportService.saveReport(report);
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/detail/" + comment.getReportId());
	}

	// comment削除処理
	@DeleteMapping("/comment/delete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

	// comment編集画面
	@GetMapping("/comment/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.editComment(id);
		mav.addObject("formModel", comment);
		mav.setViewName("/comment");
		return mav;
	}

	// comment更新処理
	@PutMapping("/comment/update/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		comment.setId(id);
		Report report = reportService.findOneReport(comment.getReportId());
		report.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
		reportService.saveReport(report);
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/");
	}

}