package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	//コメント追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	//レポートに紐づいたコメント全件取得
	public List<Comment> findByReportId(Integer id) {
		List<Comment> linkedReportId = new ArrayList<>();
		List<Comment> listAll = commentRepository.findAll(Sort.by(Sort.Direction.DESC,"updatedDate"));
		for(Comment list:listAll) {
			if(list.getReportId() == id) {
				linkedReportId.add(list);
			}
		}
		return linkedReportId;
	}

	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}
}