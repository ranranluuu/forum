package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// 全レコード取得
	public List<Report> findAllReport() {
		return reportRepository.findAllByOrderByUpdatedDateDesc();
	}

	// 日付で絞り込んだ場合
	public List<Report> searchReport(String startDate, String endDate) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		//日付の有無の確認
		if (!StringUtils.isEmpty(startDate)) {
			startDate += " 00:00:00";
		} else {
			startDate = "2022-01-10 00:00:00";
		}

		if (!StringUtils.isEmpty(endDate)) {
			endDate += " 23:59:59";
		} else {
			Date defaultDate = new Date();

            String formatDate = format.format(defaultDate);
            endDate = formatDate;
		}

		//Data型にする
		Date start = null;
		Date end = null;
		try {
			start = format.parse(startDate);
			end = format.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return reportRepository.findByUpdatedDateBetweenOrderByUpdatedDateDesc(start, end);
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	//レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	//レコード編集
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	//レコード一件取得
	public Report findOneReport(int id) {
		return reportRepository.getById(id);
	}
}
